section .data
section .text
global _start
_start:
        mov rax, rdi
        ;mov rcx, 64    ; Счетчик количества цифр
        ;sub rsp, rcx
        dec rsp
        mov byte[rsp],  0x0

        ; Деление числа и сохранение остатков в стеке
        ;mov rcx, rax         ; Загружаем количество цифр
        mov rsi, 1        
        mov rdi, 10          ; Делитель
    .num_loop:
        xor rdx, rdx
        div rdi
        add rdx, '0'   
        dec rsp    ; Преобразуем остаток в ASCII-код цифры
        mov [rsp], dl     ; Сохраняем цифру в стеке
        inc rsi
        test rax, rax
        jz .nullu
        loop .num_loop
        .nullu:
            mov rdi, rsp
        add rsp, rsi
        call print_string
        ret


        ; Вывод числа на экран
        mov rdi, 1         ; Файловый дескриптор STDOUT
        mov rsi, rsp       ; Указатель на строку
        add rsi, 1         ; Увеличиваем указатель на 1, чтобы пропустить первый элемент (знаковая цифра)
        sub rcx, 1         ; Уменьшаем количество цифр на 1, чтобы не выводить знаковую цифру
        mov rdx, rcx       ; Количество байт для вывода
        mov rax, 1      ; Системный вызов write
        syscall
        ; Восстанавливаем стек
        add rsp, 64

        ; Завершаем программу
        mov rax, 60
        xor rdi, rdi
        syscall
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.