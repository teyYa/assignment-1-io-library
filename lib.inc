section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0 ; проверка, равен ли символ нулю
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov  rdx, rax
    pop rsi ; кладем значение в rsi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10 ; основание системы счисления
    mov r8, rsp ; сохранение начального указателя стека
    push 0 ; запись в стек символа конца строки
    .loop:
        xor rdx, rdx
        div rcx ;целую часть в AC, остаток в DR
        add rdx, 0x30 ; перевод цифры в ASCII код
        dec rsp
        mov byte[rsp], dl ; запись цифры в стек
        cmp rax, 0
        je .end
        jmp .loop 
    .end:
        mov rdi, rsp
        push r8
        call print_string
        pop rsp
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint ; если неотрицательное , то вывод его как беззнакового
    push rdi ; если нет, то сохранение в стек
    mov rdi, '-' ; запись символа "минус"
    call print_char
    pop rdi ; перемещение числа в DR
    neg rdi ; изменение знака числа
    jmp print_uint ; вывод модуля числа

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rdx, rdx ; младшие - dl
    xor r8, r8 ; счётчик
    .loop:
        mov dl, byte[rdi + r8]
        cmp dl, byte[rsi + r8]
        jne .not_equals
        cmp dl, 0 ; проверка на конец строки
        je .equals ; если конец строки, то возврат 1
        inc r8
        jmp .loop
    .equals:
        mov rax, 1
        ret
    .not_equals:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp ; адрес чтения
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
	push r13
	push r14
	mov r14, rdi
	mov r12, rsi
	dec r12
	xor r13, r13

	.del_spaces:
		call read_char
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .del_spaces
		cmp rax, 0x9
		je .del_spaces
		cmp rax, 0xA
		je .del_spaces

	.loop:
		cmp r12, r13
		je .bad

		mov byte [r14+r13], al
		inc r13
		call read_char
		
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .end
		cmp rax, 0x9
		je .end
		cmp rax, 0xA
		je .end
		
		jmp .loop

	.bad:
		xor rax, rax
		pop r14
		pop r13
		pop r12
		ret

	.end:
		mov rax, r14
		mov rdx, r13
		mov byte[r14+r13], 0
		pop r14
		pop r13
		pop r12
		ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; обнуление AC
    xor rsi, rsi ; счётчик длины
    mov r8, 10 ; основание системы счисления
    .loop:
        movzx rcx, byte[rdi + rsi] ; считывание символа с расширением старших байтов
        
        cmp rcx, 0 ; проверка на конец строки
        je .end ; если конец строки, то переход в .end

        cmp cl, '0' ; проверка на то, что цифра меньше 0 (т.е. не цифра)
        jl .end ; если меньше, то переход в .end

        cmp cl, '9' ; проверка на то, что цифра больше 0 (т.е. не цифра)
        jg .end ; если больше, то переход в .end

        mul r8 ; сдвиг числа в AC влево на 1 разряд
        sub cl, 0x30 ; получение цифры из её кода в ASCII (идёт вычитание кода символа '0')
        add rax, rcx ; запись в младший разряд AC полученную цифру
        inc rsi ; счётчик длины + 1
        jmp .loop ; считывание следующего символа
    .end:
        mov rdx, rsi ; запись в DR длины числа и её возврат
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-' ; проверка на наличие символа "минус"
    je .is_negative ; если есть, то переход на .is_negative
    jmp parse_uint ; если нет, то парсить число как беззнаковое
    .is_negative:
        inc rdi ; смещение адреса начала строки на 1 (чтобы не считать минус)
        call parse_uint ; получение модуля числа
        neg rax ; изменение знака числа (дополонительный код)
        inc rdx ; добавление "минуса" к длине числа
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi ; сохранение указателя на строку
    push rsi ; а также указатель на буфер
    push rdx ; и длину буфера в стек
    call string_length ; получение длины строки
    pop rdx ; возврат значений из стека
    pop rsi
    pop rdi
    
    xor rcx, rcx ; счётчик длины

    inc rax ; + нуль-терминатор
    cmp rdx, rax ; проверка на то, что длина буфера меньше длины строки (умещается в буфере или нет)
    jl .error ; если меньше, то возврат 0

    .loop:
        cmp rcx, rax ; проверка пройдена ли вся строка
        jg .end ; если больше, то возврат длины строки
        mov r8b, [rdi + rcx] ; перемещение символа из строки в регистр r8b
        mov [rsi + rcx], r8b ; перемещение символа из регистра r8b в буфер
        inc rcx ; счётчик длины + 1
        jmp .loop
    .error:
        xor rax, rax
        ret
    .end:
        ret
